package com.nexura.calireporta.views.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.app.MyApplication;
import com.nexura.calireporta.R;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.models.ResponseCategorias2;
import com.nexura.calireporta.models.ResponseComunas2;
import com.nexura.calireporta.models.ResponseEstados2;
import com.nexura.calireporta.models.ResponseEventos2;
import com.nexura.calireporta.models.ResponseEventosAfecta2;
import com.nexura.calireporta.models.ResponseMapConfig;
import com.nexura.calireporta.models.ResponseSubCategorias2;
import com.nexura.calireporta.models.ResponseTipoValoraciones;
import com.nexura.calireporta.models.User;
import com.nexura.calireporta.models.User2;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.setAutoLogAppEventsEnabled;

public class SplashActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    SharedPrefs sharedPrefs;
    ProgressDialog pd;
    Retrofit retrofit;
    MyGovServices myGovServices;
    MyApplication _app = MyApplication.getInstance();
    String TAG = "MyGov";
    final int REQUEST_MULTIPLE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initRetrofit();
        sharedPrefs = new SharedPrefs(this);
        pd = new ProgressDialog(this);
        pd.setTitle("Cargando...");
        pd.setMessage("Por favor, espere.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.show();
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                ||
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                ||
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_WIFI_STATE)
                        != PackageManager.PERMISSION_GRANTED
                ||
                ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_MULTIPLE);
        } else {
            setNull();
            loadMapConfig();
        }
        //loadUser();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_MULTIPLE:
                if (grantResults.length > 0) {
                    boolean coarsePerm = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean finePerm = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean camPerm = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean readPerm = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    if (coarsePerm && finePerm && camPerm && readPerm) {
                        loadMapConfig();
                    } else {
                        finish();
                    }
                } else {
                    finish();
                }
                break;
        }
    }

    public void loadUser()
    {
        try
        {
            //comprueba si hay usuario loggeado sino debe dejarlo pasar sin cargar miseventos, ni meafectan
            if(!sharedPrefs.readSharedSetting("logged",false))
            {
                Log.e("LOADUSER","NO HAY USUARIO LOGGEADO");
                loadEventos();
            }
            else
            {
                    final User aux = new User();
                    aux.usuario = sharedPrefs.readSharedSetting("usuario", "");
                    String passwd = sharedPrefs.readSharedSetting("passwd", "");
                    String typeLogged = sharedPrefs.readSharedSetting("typeLogged","");
                    String nombre = sharedPrefs.readSharedSetting("nombre","");
                    String apellido = sharedPrefs.readSharedSetting("apellido","");
        
                    String token_google = sharedPrefs.readSharedSetting("token_google","");
                    Log.e("LOADUSER", "ANTES DE SERVICIO");
                    if(typeLogged.contains("GOOGLE")){
                        myGovServices.signinGoogle(nombre,apellido,aux.usuario,token_google).enqueue(new Callback<User2>() {
                            @Override
                            public void onResponse (Call<User2> call, Response<User2> response) {
                                sharedPrefs.saveSharedSetting("token", response.body().data.token);
                                aux.token = response.body().data.token;
                                aux.success = true;
                                MyApplication.setUser(aux);
                                loadMisEventos();
                            }
    
                            @Override
                            public void onFailure (Call<User2> call, Throwable throwable) {
        
                            }
                        });
                    } else {
                        myGovServices.login(typeLogged, aux.usuario, passwd).enqueue(new Callback<User2>() {
                            @Override
                            public void onResponse(Call<User2> call, Response<User2> response) {
                                Log.e("LOADUSER", "USUARIO LOGGEADO");
                                if (response.isSuccessful()) {
                                    aux.token = response.body().data.token;
                                    aux.success = true;
                                    Log.e("LOADUSER", "USUARIO LOGGEADO");
                                    Log.e("TOKEN", aux.token);
                                    Log.e("USUARIO", aux.usuario);
                                    Log.e("LOADUSER", "SE HAN CARGADO LOS DATOS DEL USUARIO");
                                    sharedPrefs.saveSharedSetting("token", aux.token);
                                    MyApplication.setUser(aux);
                                    loadMisEventos();
                                }
                            }
                            @Override
                            public void onFailure(Call<User2> call, Throwable t) {
                            }
                        });
                    }

            }

        }
        catch (Exception e)
        {
            Log.e("ERR - loadUser",e.toString());
        }
    }

    public void loadEventos()
    {
        try
        {
            if(!sharedPrefs.readSharedSetting("loadEventos",false))
            {
                Call<ResponseEventos2> getEventos = myGovServices.getEventos();
                getEventos.enqueue(new Callback<ResponseEventos2>() {
                    @Override
                    public void onResponse(Call<ResponseEventos2> call, Response<ResponseEventos2> response) {
                        if(response.isSuccessful()) {
                            try {
                                MyApplication.setEventos(response.body().data.eventos);
                                MyApplication.setEventosBackup(response.body().data.eventos);
                            } catch (Exception e) {
                                Log.d(TAG, e.toString());
                            }
                            //pd.dismiss();
                            //initMain();
                            //loadComunas();
                            Gson gson = new Gson();
                            ResponseEventos2 resp = response.body();
                            String events = gson.toJson(resp);
                            sharedPrefs.saveSharedSetting("loadEventos", true);
                            sharedPrefs.saveSharedSetting("eventos", events);
                            loadComunas();
                        }else{
                            loadEventos();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseEventos2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " EVENTS-E", t.toString());
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("eventos",null);
                if(stored!=null)
                {

                }
                ResponseEventos2 res = gson.fromJson(stored,ResponseEventos2.class);
                MyApplication.setEventos(res.data.eventos);
                MyApplication.setEventosBackup(res.data.eventos);
                Log.e("LOADEVENTOS","SE HAN CARGADO LOS EVENTOS GUARDADOS");
                loadComunas();
            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadEventos",e.toString());
        }
    }

    public void loadMisEventos()
    {
        try
        {
            Log.e("LoadMisEventos","Before isOnline");
            if (!sharedPrefs.readSharedSetting("loadMisEventos",false))
            {
                Log.e("LoadMisEventos","After isOnline");
    
                Call<ResponseEventos2> getEventos = myGovServices.getEventosMios(sharedPrefs.readSharedSetting("token",""));
                getEventos.enqueue(new Callback<ResponseEventos2>() {
                    @Override
                    public void onResponse(Call<ResponseEventos2> call, Response<ResponseEventos2> response) {
                        if(response.isSuccessful())
                        {
                            try
                            {
                                Log.e("LoadMisEventos","success");
    
                                MyApplication.setMisEventos(response.body().data.eventos);
                                MyApplication.setMisEventosBackup(response.body().data.eventos);
                            }
                            catch (Exception e)
                            {
                                Log.e("LoadMisEventos","error");
    
                                Log.d(TAG,e.toString());
                            }
                            //pd.dismiss();
                            //initMain();

                            Gson gson = new Gson();
                            ResponseEventos2 resp = response.body();
                            String events = gson.toJson(resp);
                            sharedPrefs.saveSharedSetting("loadMisEventos",true);
                            sharedPrefs.saveSharedSetting("misEventos",events);
                            loadMeafecta();

                        }else{
                            MyApplication.setMisEventos(new ArrayList());
                            MyApplication.setMisEventosBackup(new ArrayList());
                            Log.e("LoadMisEventos","error");
                            Log.e("LOADMISEVENTOS",response.message());
                            loadMeafecta();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEventos2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " EVENTS-E", t.toString());
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("misEventos",null);
                if(stored!=null)
                {

                }
                ResponseEventos2 res = gson.fromJson(stored,ResponseEventos2.class);
                MyApplication.setMisEventosBackup(res.data.eventos);
                MyApplication.setMisEventos(res.data.eventos);
                Log.e("LOADMISEVENTOS","SE HAN CARGADO MIS EVENETOS GUARDADOS");
                loadMeafecta();
                Log.e("LoadMisEventos","offline");
            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadMisEventos",e.toString());
        }


    }

    public void loadMeafecta()
    {
        try
        {
            Log.e("Entra Me afecta","intenta entrar");
    
            if (isOnline(this))
            {
                Log.e("Entra a isOnline","Verdadero");
                Call<ResponseEventosAfecta2> getEventos = myGovServices.getEventosAfecta(MyApplication.getUser().token);
                getEventos.enqueue(new Callback<ResponseEventosAfecta2>() {
                    @Override
                    public void onResponse(Call<ResponseEventosAfecta2> call, Response<ResponseEventosAfecta2> response) {
                        if(response.isSuccessful())
                        {
                            try
                            {
                                Log.e("Entra Me afecta","success");
    
                                MyApplication.setEventosMeAfecta(response.body().data.eveAfecta);
                                MyApplication.setEventosMeAfectaBackup(response.body().data.eveAfecta);
                                Gson gson = new Gson();
                                ResponseEventosAfecta2 resp = response.body();
                                String events = gson.toJson(resp);
                                sharedPrefs.saveSharedSetting("loadMeAfecta",true);
                                sharedPrefs.saveSharedSetting("meAfecta",events);
                                loadEventos();
                            }
                            catch (Exception e)
                            {
                                Log.d(TAG,e.toString());
                                Log.e("Entra Me afecta","catch");
    
                            }
                            //pd.dismiss();
                            //initMain();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEventosAfecta2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " EVENTS-E", t.toString());
                        
                    }
                });
            }
            else
            {
                Log.e("Entra Me afecta","Desde datos almacenados");
    
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("meAfecta",null);
                Log.d("meAfecta","ENTRO");
                ResponseEventosAfecta2 res = gson.fromJson(stored,ResponseEventosAfecta2.class);
                MyApplication.setEventosMeAfecta(res.data.eveAfecta);
                MyApplication.setEventosMeAfectaBackup(res.data.eveAfecta);
                Log.e("LOADMEAFECTA","SE HAN CARGADO LOS EVENTOS MEAFECTA GUARDADOS");
                loadEventos();
            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadMeAfecta",e.toString());
            Log.e("ERR - loadMeAfecta",e.getMessage());
        }

    }

    public void loadComunas()
    {
        try
        {
            if(!sharedPrefs.readSharedSetting("loadComunas",false))
            {
                Call<ResponseComunas2> getComunas = myGovServices.getComunas();
                getComunas.enqueue(new Callback<ResponseComunas2>() {
                    @Override
                    public void onResponse(Call<ResponseComunas2> call, Response<ResponseComunas2> response) {

                        MyApplication.setComunas(response.body().data.comunas);
                        Gson gson = new Gson();
                        ResponseComunas2 resp = response.body();
                        String events = gson.toJson(resp);
                        sharedPrefs.saveSharedSetting("loadComunas",true);
                        sharedPrefs.saveSharedSetting("comunas",events);
                        loadCategorias();
                    }

                    @Override
                    public void onFailure(Call<ResponseComunas2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("comunas",null);
                Log.d(TAG,"ENTRE else comunas");
                ResponseComunas2 res = gson.fromJson(stored,ResponseComunas2.class);
                MyApplication.setComunas(res.data.comunas);
                Log.e("LOADCOMUNAS","SE HAN CARGADO LAS COMUNAS GUARDADAS ");
                loadCategorias();
            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadComunas",e.toString());
        }
    }

    public void loadCategorias()
    {
        try
        {
            if(!sharedPrefs.readSharedSetting("loadCategorias",false))
            {
                Call<ResponseCategorias2> getCategorias = myGovServices.getCategorias();
                getCategorias.enqueue(new Callback<ResponseCategorias2>() {
                    @Override
                    public void onResponse(Call<ResponseCategorias2> call, Response<ResponseCategorias2> response) {
                        MyApplication.setCategorias(response.body().data.categorias);
                        Gson gson = new Gson();
                        ResponseCategorias2 resp = response.body();
                        String events = gson.toJson(resp);
                        sharedPrefs.saveSharedSetting("loadCategorias",true);
                        sharedPrefs.saveSharedSetting("categorias",events);
                        loadSubCategorias();
                    }

                    @Override
                    public void onFailure(Call<ResponseCategorias2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " CAT-C", t.toString());
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("categorias",null);
                if(stored!=null)
                {

                }
                ResponseCategorias2 res = gson.fromJson(stored,ResponseCategorias2.class);
                MyApplication.setCategorias(res.data.categorias);
                Log.e("LOADCATEGORIAS","SE HAN CARGADO LAS CATEGORIAS GUARDADAS");
                Log.e("LOADCATEGORIAS",stored);
                loadSubCategorias();
            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadCategorias",e.toString());
        }

    }

    public void loadSubCategorias()
    {
        try
        {
            if(!sharedPrefs.readSharedSetting("loadSubCategorias",false))
            {
                Call<ResponseSubCategorias2> getSubCategorias = myGovServices.getSubCategorias();
                getSubCategorias.enqueue(new Callback<ResponseSubCategorias2>() {
                    @Override
                    public void onResponse(Call<ResponseSubCategorias2> call, Response<ResponseSubCategorias2> response) {
                        MyApplication.setSubcategorias(response.body().data.subcategorias);
                        Gson gson = new Gson();
                        ResponseSubCategorias2 resp = response.body();
                        String subcategorias = gson.toJson(resp);
                        sharedPrefs.saveSharedSetting("loadSubCategorias",true);
                        sharedPrefs.saveSharedSetting("subcategorias",subcategorias);
                        loadEstados();
                    }

                    @Override
                    public void onFailure(Call<ResponseSubCategorias2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " CAT-C", t.toString());
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("subcategorias",null);
                if(stored!=null)
                {

                }
                ResponseSubCategorias2 res = gson.fromJson(stored,ResponseSubCategorias2.class);
                MyApplication.setSubcategorias(res.data.subcategorias);
                Log.e("LOADSUBCATEGORIAS","SE HAN CARGADO LAS SUBCATEGORIAS GUARDADAS");
                loadEstados();

            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadSubCategorias",e.toString());
        }


    }

    public void loadEstados()
    {

        try {
            if (!sharedPrefs.readSharedSetting("loadEstados", false)) {
                Call<ResponseEstados2> getEstados = myGovServices.getEstados();
                getEstados.enqueue(new Callback<ResponseEstados2>() {
                    @Override
                    public void onResponse(Call<ResponseEstados2> call, Response<ResponseEstados2> response) {
                        MyApplication.setEstados(response.body().data.estados);
                        sharedPrefs.saveSharedSetting("loadEstados",true);
                        Gson gson = new Gson();
                        ResponseEstados2 resp = response.body();
                        String estados = gson.toJson(resp);
                        sharedPrefs.saveSharedSetting("estados",estados);
                        loadTipoValoraciones();
                    }

                    @Override
                    public void onFailure(Call<ResponseEstados2> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{

                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("estados",null);
                if(stored!=null)
                {

                }
                ResponseEstados2 responseEstados = gson.fromJson(stored,ResponseEstados2.class);
                MyApplication.setEstados(responseEstados.data.estados);
                Log.e("LOADESTADOS","SE HAN CARGADO LOS ESTADOS GUARDADOS");
                loadTipoValoraciones();

            }
        }
        catch(Exception e)
            {
                Log.d("ERR - estados", e.toString());
            }


    }

    public void loadTipoValoraciones()
    {
        try
        {
            if(!sharedPrefs.readSharedSetting("loadTipoValoraciones",false)) {
                Call<ResponseTipoValoraciones> getValoraciones = myGovServices.getTipoValoraciones();
                getValoraciones.enqueue(new Callback<ResponseTipoValoraciones>() {
                    @Override
                    public void onResponse(Call<ResponseTipoValoraciones> call, Response<ResponseTipoValoraciones> response) {
                        MyApplication.setValoraciones(response.body().tiposValoracion);
                        sharedPrefs.saveSharedSetting("loadTipoValoraciones",true);
                        Gson gson = new Gson();
                        ResponseTipoValoraciones responseTipoValoraciones = response.body();
                        String tipoValoraciones = gson.toJson(responseTipoValoraciones);
                        sharedPrefs.saveSharedSetting("tipovaloraciones",tipoValoraciones);
                        pd.dismiss();
                        initMain();
                    }

                    @Override
                    public void onFailure(Call<ResponseTipoValoraciones> call, Throwable t) {
                        Toast.makeText(SplashActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        initMain();
                    }
                });
            }else{
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("tipovaloraciones",null);
                if(stored!=null)
                {

                }
                ResponseTipoValoraciones responseTipoValoraciones = gson.fromJson(stored,ResponseTipoValoraciones.class);
                MyApplication.setValoraciones(responseTipoValoraciones.tiposValoracion);
                Log.e("LOADTIPOSVALORACION","SE HAN CARGADO LOS TIPOS DE VALORACION");
                pd.dismiss();
                initMain();
            }
        }
        catch (Exception e)
        {
            Log.d("ERR - valoraciones",e.toString());
            initMain();
        }
    }
    public void loadMapConfig(){
        Call<ResponseMapConfig> responseMapConfigCall = myGovServices.getMapConfig();
        responseMapConfigCall.enqueue(new Callback<ResponseMapConfig>() {
            @Override
            public void onResponse (Call<ResponseMapConfig> call, Response<ResponseMapConfig> response) {
                MyApplication.setMapConfig(response.body().data);
                sharedPrefs.saveSharedSetting("loadMapConfig",true);
                Gson gson = new Gson();
                ResponseMapConfig responseMapConfig = response.body();
                String mapConfig = gson.toJson(responseMapConfig);
                Log.e("MapConfig",mapConfig);
                sharedPrefs.saveSharedSetting("mapconfig",mapConfig);
                loadUser();
            }
    
            @Override
            public void onFailure (Call<ResponseMapConfig> call, Throwable throwable) {
                loadUser();
            }
        });
    }
    public void initRetrofit()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.server))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myGovServices = retrofit.create(MyGovServices.class);
    }

    public void initMain()
    {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        finish();
        /*
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 3000);
        
         */
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
    public void setNull(){
        sharedPrefs.saveSharedSetting("loadEventos", false);
        sharedPrefs.saveSharedSetting("eventos", null);
        sharedPrefs.saveSharedSetting("loadMisEventos",false);
        sharedPrefs.saveSharedSetting("misEventos",null);
        sharedPrefs.saveSharedSetting("loadMeAfecta",false);
        sharedPrefs.saveSharedSetting("meAfecta",null);
        sharedPrefs.saveSharedSetting("loadComunas",false);
        sharedPrefs.saveSharedSetting("comunas",null);
        sharedPrefs.saveSharedSetting("loadCategorias",false);
        sharedPrefs.saveSharedSetting("categorias",null);
        sharedPrefs.saveSharedSetting("loadSubCategorias",false);
        sharedPrefs.saveSharedSetting("subcategorias",null);
        sharedPrefs.saveSharedSetting("loadEstados",false);
        sharedPrefs.saveSharedSetting("estados",null);
        sharedPrefs.saveSharedSetting("loadTipoValoraciones",false);
        sharedPrefs.saveSharedSetting("tipovaloraciones",null);
        sharedPrefs.saveSharedSetting("loadMapConfig",false);
        sharedPrefs.saveSharedSetting("mapconfig",null);
    }
}
