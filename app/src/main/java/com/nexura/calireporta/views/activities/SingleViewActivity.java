package com.nexura.calireporta.views.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.nexura.calireporta.R;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.adapters.ComentariosAdapter;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.app.MyApplication;
import com.nexura.calireporta.models.EventoSingle;
import com.nexura.calireporta.models.ResponseEvento2;
import com.nexura.calireporta.models.ResponseEventosAfecta2;
import com.nexura.calireporta.models.ResponseImage;
import com.nexura.calireporta.models.ResponsecomentarEvento2;
import com.nexura.calireporta.models.ResponsevalorarEvento2;
import com.nexura.calireporta.utils.Utils;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.nexura.calireporta.views.activities.NewCaseActivity.encodeToBase64;

public class SingleViewActivity extends AppCompatActivity implements OnMapReadyCallback{
    String email_string;
    LoginButton loginButton;
    CallbackManager callbackManager;
    private ProgressDialog dialog;
    Context contextP;
    private TextInputEditText email,password;
    private int LOGIN_REQUEST=111;
    SharedPrefs sharedPrefs;
    private SliderLayout mDemoSlider;
    ProgressDialog pd;
    Retrofit retrofit;
    MyGovServices myGovServices;
    String TAG = "MyGov";
    MapView mMapView;
    ScrollView scrollView;
    FancyButton addComentario,meAfecta;
    private GoogleMap mMap;
    boolean markerCreated = false;
    boolean dataloaded = false;
    EventoSingle eventoSingle;
    boolean mapready = false;
    double lat,lng;
    int estadoBotonComentario = 0;
    int id;
    RecyclerView recyclerView_comentarios;
    RecyclerView.LayoutManager layoutManager;
    ComentariosAdapter adapterComentarios;
    LinearLayout linearLayoutComentarios;
    public int COMENTARIO_OK = 222;
    ImageButton imgComentario;
    private ArrayList<Image> images = new ArrayList<>();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initRetrofit();
        imgComentario = findViewById(R.id.imgComentario);
        contextP = this;
        dialog = new ProgressDialog(this);
        sharedPrefs = new SharedPrefs(getApplicationContext());
        scrollView=(ScrollView)findViewById(R.id.scrollView);
        pd = new ProgressDialog(this);
        pd.setTitle("Cargando...");
        pd.setMessage("Por favor, espere.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.show();
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        //mMapView.onResume(); // needed to get the map to display immediately
        mMapView.getMapAsync(this);
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent i = getIntent();
        id = i.getIntExtra("id",0);
        int from = i.getIntExtra("from",0); //0 for recent
        Log.d("MYGOVID",""+String.valueOf(id));
        //int
        if(from==0)
        {
            if(sharedPrefs.readSharedSetting("logged",false))
                myGovServices.getEvento(MyApplication.getUser().token,id).enqueue(new Callback<ResponseEvento2>() {
                    @Override
                    public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                        setData(response.body().data.evento);
                        eventoSingle = response.body().data.evento;
                        if (eventoSingle.mi_valoracion == 1) {
                            meAfecta.setEnabled(false);
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                        setData(MyApplication.getEventos().get(id));
                    }
                });
            else{

                myGovServices.getEvento(id).enqueue(new Callback<ResponseEvento2>() {
                    @Override
                    public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                        setData(response.body().data.evento);
                        eventoSingle = response.body().data.evento;
                    }
                    @Override
                    public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                        setData(MyApplication.getEventos().get(id));
                    }
                });
            }
        }
        else if(from==1)
        {
            if(sharedPrefs.readSharedSetting("logged",false)){
                myGovServices.getEvento(MyApplication.getUser().token,id).enqueue(new Callback<ResponseEvento2>() {
                    @Override
                    public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                        setData(response.body().data.evento);
                        eventoSingle = response.body().data.evento;
                        if (eventoSingle.mi_valoracion == 1) {
                            meAfecta.setEnabled(false);
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                        setData(MyApplication.getEventos().get(id));
                    }
                });
            } else {
                myGovServices.getEvento(id).enqueue(new Callback<ResponseEvento2>() {
                    @Override
                    public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                        setData(response.body().data.evento);
                        eventoSingle = response.body().data.evento;
                        
                    }
                    @Override
                    public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                        setData(MyApplication.getEventos().get(id));
                    }
                });
            }

        }
        else if(from==2)
        {
            myGovServices.getEvento(MyApplication.getUser().token,id).enqueue(new Callback<ResponseEvento2>() {
                @Override
                public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                    setData(response.body().data.evento);
                    eventoSingle = response.body().data.evento;
                    if (eventoSingle.mi_valoracion == 1) {
                        meAfecta.setEnabled(false);
                    }
                }
                @Override
                public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                    setData(MyApplication.getMisEventos().get(MyApplication.getPosicionMisEventosconId(id)));
                }
            });

        }
        else if(from==3)
        {
            myGovServices.getEvento(MyApplication.getUser().token,id).enqueue(new Callback<ResponseEvento2>() {
                @Override
                public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                    setData(response.body().data.evento);
                    eventoSingle = response.body().data.evento;
                    if (eventoSingle.mi_valoracion == 1) {
                        meAfecta.setEnabled(false);
                    }
                }
                @Override
                public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                    setData(MyApplication.getEventosMeAfecta().get(MyApplication.getPosicionmeAfectaconId(id)));
                }
            });


        }
        mMapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return mMapView.onTouchEvent(event);
            }
        });
        callbackManager = CallbackManager.Factory.create();

    }

    public void initRetrofit()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.server))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myGovServices = retrofit.create(MyGovServices.class);
    }

    public void setData(final EventoSingle evt)
    {
            setupImages(evt.archivos);
        lat=Double.parseDouble(evt.y);
        lng=Double.parseDouble(evt.x);
        dataloaded=true;
        TextView id_texto = (TextView)findViewById(R.id.id);
        TextView titulo = (TextView)findViewById(R.id.titulo);
        TextView descripcion = (TextView)findViewById(R.id.description);
        TextView fecha = (TextView)findViewById(R.id.fecha);
        TextView categoria = (TextView)findViewById(R.id.categoria);
        TextView estado = (TextView)findViewById(R.id.estado);
        TextView barrio = (TextView)findViewById(R.id.barrio);
        final TextView afectados = (TextView)findViewById(R.id.afectados);
        final EditText comentario = (EditText)findViewById(R.id.comentario);
        linearLayoutComentarios = findViewById(R.id.linearComentarios);
        recyclerView_comentarios = (RecyclerView) findViewById(R.id.lista_comentarios);
        recyclerView_comentarios.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView_comentarios.setLayoutManager(layoutManager);
        final ExpandableLinearLayout expandableComentario = (ExpandableLinearLayout) findViewById(R.id.expandableComentario);
        addComentario = (FancyButton) findViewById(R.id.addcomentario);
        meAfecta = (FancyButton) findViewById(R.id.meafecta);
        if(evt.comentarios.isEmpty()){
            linearLayoutComentarios.setVisibility(View.GONE);
        }else {
            linearLayoutComentarios.setVisibility(View.VISIBLE);
        }
        meAfecta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPrefs.readSharedSetting("logged",false)){

                    if(evt.mi_valoracion == 1) {
                        meAfecta.setEnabled(false);

                    }
                    myGovServices.valorarEvento(MyApplication.getUser().token,id,1).enqueue(new Callback< ResponsevalorarEvento2> (){

                        @Override
                        public void onResponse(Call<ResponsevalorarEvento2> call, Response<ResponsevalorarEvento2> response) {
                            if (response.isSuccessful()){
                                if(response.code() != 500) {
                                    if (response.body().data.success)
                                        if (!response.body().data.valoracion) {
                                            meAfecta.setEnabled(false);
                                            evt.me_afecta_cuenta++;
                                            afectados.setText(String.valueOf(evt.me_afecta_cuenta));
                                            Toast.makeText(contextP, "CASO VALORADO", Toast.LENGTH_LONG).show();
                                            myGovServices.getEventosAfecta(MyApplication.getUser().token).enqueue(new Callback<ResponseEventosAfecta2>() {
                                                @Override
                                                public void onResponse(Call<ResponseEventosAfecta2> call, Response<ResponseEventosAfecta2> response) {
                                                    MyApplication.setEventosMeAfecta(response.body().data.eveAfecta);
                                                    MyApplication.setEventosMeAfectaBackup(response.body().data.eveAfecta);
                                                    Gson gson = new Gson();
                                                    ResponseEventosAfecta2 resp = response.body();
                                                    String events = gson.toJson(resp);
                                                    sharedPrefs.saveSharedSetting("loadMeAfecta", true);
                                                    sharedPrefs.saveSharedSetting("meAfecta", events);
                                                    setResult(RESULT_OK);
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseEventosAfecta2> call, Throwable t) {
                                                    Gson gson = new Gson();
                                                    String stored = sharedPrefs.readSharedSetting("meAfecta", null);
                                                    Log.d("meAfecta", "ENTRO");
                                                    ResponseEventosAfecta2 res = gson.fromJson(stored, ResponseEventosAfecta2.class);
                                                    MyApplication.setEventosMeAfecta(res.data.eveAfecta);
                                                    MyApplication.setEventosMeAfectaBackup(res.data.eveAfecta);
                                                    Log.e("LOADMEAFECTA", "SE HAN CARGADO LOS EVENTOS MEAFECTA GUARDADOS");
                                                }
                                            });
                                        }
                                }else if(response.code() == 500){
                                    Toast.makeText(contextP,response.message().toString(),Toast.LENGTH_SHORT).show();
                                    meAfecta.setEnabled(false);

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponsevalorarEvento2> call, Throwable t) {

                        }
                    });

                    //EVENTO ME AFECTA

                }else{
                    //SI NO HAY USUARIO LOGGEADO
                    //PANTALLA DE LOGIN
                    Utils.setIntent(contextP,LoginActivity.class);
                }
            }
        });

        addComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (estadoBotonComentario == 0) {
                    linearLayoutComentarios.setVisibility(View.VISIBLE);
                    expandableComentario.toggle();
                    addComentario.setText("Enviar Comentario");
                    estadoBotonComentario = 1;
                }else if(estadoBotonComentario == 1){
                    if (comentario.getText().length() == 0)
                        Toast.makeText(SingleViewActivity.this,"Campo de comentario vacío",Toast.LENGTH_LONG).show();
                    else{
                        if(sharedPrefs.readSharedSetting("logged",false)){// SI HAY USUARIO LOGGEADO
                            //ENVIAR COMENTARIO
                            addComentario.setEnabled(false);
                            myGovServices.comentarEvento(MyApplication.getUser().token,String.valueOf(id),comentario.getText().toString()).enqueue(new Callback<ResponsecomentarEvento2>() {
                                @Override
                                public void onResponse(Call<ResponsecomentarEvento2> call, Response<ResponsecomentarEvento2> response) {
                                    if(response.body().data.success) {
                                        Toast.makeText(contextP, "SE HA COMENTADO EL CASO ... ", Toast.LENGTH_LONG).show();
                                        comentario.setText("");
                                        Log.e("NEWEVENTACTIVITY","CARGANDO IMAGENES ..."+ String.valueOf(0));
                                        if(images.size() > 0){
                                        

                                        Bitmap ima = BitmapFactory.decodeFile(images.get(0).getPath());
                                        String im = encodeToBase64(ima, Bitmap.CompressFormat.JPEG,50);
                                        Call<ResponseImage> uploadImage = myGovServices.createImg(sharedPrefs.readSharedSetting("token",""),id+"",im);
                                        uploadImage.enqueue(new Callback<ResponseImage>() {
                                            @Override
                                            public void onResponse(Call<ResponseImage> call, Response<ResponseImage> response) {
                                                imgComentario.setImageDrawable(getResources().getDrawable(R.drawable.baseline_add_a_photo_black_48dp));
                                                images.clear();
                                                if(sharedPrefs.readSharedSetting("logged",false)){
                                                    myGovServices.getEvento(MyApplication.getUser().token,id).enqueue(new Callback<ResponseEvento2>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                                                            setData(response.body().data.evento);
                                                            addComentario.setEnabled(true);
                                                            setResult(RESULT_OK);
                                                        }
            
                                                        @Override
                                                        public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                
                                                        }
                                                    });
                                                }else{
                                                    myGovServices.getEvento(id).enqueue(new Callback<ResponseEvento2>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                                                            setData(response.body().data.evento);
                                                        }
            
                                                        @Override
                                                        public void onFailure(Call<ResponseEvento2> call, Throwable t) {
                
                                                        }
                                                    });
        
                                                }
                                            }
                                            @Override
                                            public void onFailure(Call<ResponseImage> call, Throwable t) {
                                            }
                                        });
                                        } else {
                                            myGovServices.getEvento(MyApplication.getUser().token,id).enqueue(new Callback<ResponseEvento2>() {
                                                @Override
                                                public void onResponse(Call<ResponseEvento2> call, Response<ResponseEvento2> response) {
                                                    setData(response.body().data.evento);
                                                    addComentario.setEnabled(true);
                                                    setResult(RESULT_OK);
    
                                                }
        
                                                @Override
                                                public void onFailure(Call<ResponseEvento2> call, Throwable t) {
            
                                                }
                                            });
                                        }

                                    }
                                    else
                                        Toast.makeText(contextP,"NO SE PUDO COMENTAR EL CASO ... ",Toast.LENGTH_LONG).show();

                                }

                                @Override
                                public void onFailure(Call<ResponsecomentarEvento2> call, Throwable t) {

                                }
                            });


                        }else{//SI NO HAY USUARIO LOGGEADO
                            //PANTALLA DE LOGIN
                            Utils.setIntent(contextP,LoginActivity.class);
                        }
                    }
                }
            }
        });
        //Toast.makeText(this, evt.titulo + evt.descripcion + evt.categoria.nombre, Toast.LENGTH_SHORT).show();
        id_texto.setText("ID: "+evt.id);
        titulo.setText(evt.titulo);
        descripcion.setText(evt.descripcion);
        fecha.setText(evt.fecha);
        categoria.setText(evt.categoria.nombre);
        estado.setText(evt.estado.nombre);
        barrio.setText(evt.localidad.nombre);
        if(evt.mi_valoracion >0)
            meAfecta.setEnabled(false);
        afectados.setText(String.valueOf(evt.me_afecta_cuenta));
        adapterComentarios = new ComentariosAdapter(this,evt.comentarios);
        recyclerView_comentarios.setAdapter(adapterComentarios);
        if(mapready&&!markerCreated)
        {
            Log.d(TAG,"entro en data");
            LatLng sydney = new LatLng(lat, lng);
            mMap.addMarker(new MarkerOptions().position(sydney));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,18));
            markerCreated=true;
        }
        pd.dismiss();

    }


    public void setupImages(List<String> images)
    {
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        mDemoSlider.removeAllSliders();
    
        HashMap<String,String> url_maps = new HashMap<String, String>();
        if(images == null){
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            // initialize a SliderLayout
            textSliderView
                  .image(R.drawable.sin_imagen)
                  .setScaleType(BaseSliderView.ScaleType.Fit);
            mDemoSlider.addSlider(textSliderView);
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.stopAutoCycle();
            return;
        }
        for ( String url : images)
        {
            url_maps.put(url,getResources().getString(R.string.server)+url);
        }
        for(String name : url_maps.keySet()){
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            
                    //.setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);
            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.stopAutoCycle();
        //mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        //mDemoSlider.setDuration(4000);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapready=true;
        mMap = googleMap;
        if(dataloaded&&!markerCreated)
        {
            Log.d(TAG,"entro en mapa");
            LatLng sydney = new LatLng(lat, lng);
            mMap.addMarker(new MarkerOptions().position(sydney));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,18));
            markerCreated=true;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        if (requestCode == 321 && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> image = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            images = image;
            Log.e("NEWEVENTACTIVITY","TAMAÑO"+String.valueOf(image.size()));
            if(image.size()>0){
                    Bitmap ima = BitmapFactory.decodeFile(image.get(0).getPath());
                    imgComentario.setImageBitmap(ima);

            }
        }
    }
    public void openPhotoPicker(View view)
    {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_MODE, true);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_MULTIPLE);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_LIMIT, 1);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SHOW_CAMERA, true);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES, images);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_TITLE, "Carpetas");
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_TITLE, "Selecciona la imagen");
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_DIRECTORY, "Cámara");
        startActivityForResult(intent, 321);
    }
}
