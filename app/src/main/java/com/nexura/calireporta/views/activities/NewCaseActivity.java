package com.nexura.calireporta.views.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nexura.calireporta.R;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.app.MyApplication;
import com.nexura.calireporta.models.Categoria;
import com.nexura.calireporta.models.Error;
import com.nexura.calireporta.models.MapConfig;
import com.nexura.calireporta.models.NuevoEvento;
import com.nexura.calireporta.models.ResponseAllowZone;
import com.nexura.calireporta.models.ResponseCategorias2;
import com.nexura.calireporta.models.ResponseImage;
import com.nexura.calireporta.models.ResponseNuevoEvento2;
import com.nexura.calireporta.models.SubCategoria;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.utils.WorkaroundMapFragment;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewCaseActivity extends AppCompatActivity implements OnMapReadyCallback{
    int REQUEST_NEW_EVENT=345;
    GoogleMap mMap;
    ScrollView mScrollView;
    WorkaroundMapFragment mapFragment;
    
    
    double lat,lon;
    private ArrayList<Image> images = new ArrayList<>();
    private List<ImageView> imgs = new ArrayList<>();
    Spinner categoria,subcategoria;
    NuevoEvento nuevo = new NuevoEvento();
    ProgressDialog pd;
    Retrofit retrofit;
    MyGovServices myGovServices;
    String TAG = "MyGov";
    SharedPrefs sharedPrefs;
    TextInputEditText titulo,descripcion;
    FancyButton enviar;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        context = this;
        mScrollView = findViewById(R.id.scrollView);
        sharedPrefs= new SharedPrefs(this);
        mapFragment = (WorkaroundMapFragment) getSupportFragmentManager()
              .findFragmentById(R.id.map);
        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
        mapFragment.getMapAsync(this);
        
        Intent data = getIntent();
        MapConfig mapConfig = MyApplication.getMapConfig();
        lat=data.getDoubleExtra("lat",Double.valueOf(mapConfig.map.lat));
        lon = data.getDoubleExtra("lon",Double.valueOf(mapConfig.map.lng));
        ImageView img1 = (ImageView)findViewById(R.id.img1);
        ImageView img2 = (ImageView)findViewById(R.id.img2);
        ImageView img3 = (ImageView)findViewById(R.id.img3);
        ImageView img4 = (ImageView)findViewById(R.id.img4);
        ImageView img5 = (ImageView)findViewById(R.id.img5);
        enviar = findViewById(R.id.enviar);
        imgs.add(img1);
        imgs.add(img2);
        imgs.add(img3);
        imgs.add(img4);
        imgs.add(img5);
        titulo = (TextInputEditText)findViewById(R.id.titulo);
        descripcion = (TextInputEditText)findViewById(R.id.descripcion);
        categoria = (Spinner)findViewById(R.id.categoria);
        subcategoria = (Spinner)findViewById(R.id.subcategoria);
        List<String> dataCategoria = new ArrayList<>();
        Gson gson = new Gson();
        String stored = sharedPrefs.readSharedSetting("categorias",null);
        if(stored!=null)
        {

        }
        ResponseCategorias2 res = gson.fromJson(stored,ResponseCategorias2.class);
        MyApplication.setCategorias(res.data.categorias);
        for (int i = 0; i < MyApplication.getCategorias().size(); i++) {
            Categoria c = MyApplication.getCategorias().get(i);
            dataCategoria.add(c.nombre);
            Log.d("CATS",c.nombre);
        }
        ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(
                getApplicationContext(), android.R.layout.simple_spinner_item, dataCategoria);
        adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoria.setAdapter(adapterCat);
        categoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                ((TextView) view).setTextColor(Color.BLACK);
                loadSubCat(selectedItemText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initRetrofit();
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                validateIsAllow();
            }
        });
    }

    public void loadSubCat(String cat)
    {

        for (Categoria x:MyApplication.getCategorias())
            {
                if(x.nombre.equals(cat))
                {
                    ArrayAdapter<SubCategoria> adapterCat = new ArrayAdapter<SubCategoria>(
                            getApplicationContext(), android.R.layout.simple_spinner_item, x.subcategorias);
                    adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    subcategoria.setAdapter(adapterCat);
                    subcategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ((TextView) view).setTextColor(Color.BLACK);
                            SubCategoria selectedItemText = (SubCategoria) parent.getItemAtPosition(position);
                            //Toast.makeText(NewCaseActivity.this, selectedItemText.id+"", Toast.LENGTH_SHORT).show();
                            nuevo.categoria_id=selectedItemText.id+"";
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    break;
                }
            }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapFragment.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapFragment.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapFragment.onPause();
    }

    @Override
    public void onDestroy() {
        //mapFragment.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(lat, lon);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,18));
    }

    public void openPhotoPicker(View view)
    {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_MODE, true);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_MULTIPLE);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_LIMIT, 5);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SHOW_CAMERA, true);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES, images);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_TITLE, "Carpetas");
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_TITLE, "Selecciona las imágenes");
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_DIRECTORY, "Cámara");
        //intent.putExtra(ImagePickerActivity, true); //default is false
        startActivityForResult(intent, 321);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == 321 && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> image = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            images = image;
            Log.e("NEWEVENTACTIVITY","TAMAÑO"+String.valueOf(image.size()));
            if(image.size()>0){
                for (int i = 0;i<image.size();i++)
                {
                    imgs.get(i).setImageResource(android.R.color.transparent);
                    Log.e("NEWEVENTACTIVITY","Coloca imagenes transparentes sin p2");
                }
                for(int i = 0; i<image.size();i++)
                {
                    Bitmap ima = BitmapFactory.decodeFile(image.get(i).getPath());
                    imgs.get(i).setImageBitmap(ima);
                    Log.e("NEWEVENTACTIVITY","Coloca imagenes en array");

                }
            }
        }
    }

    public void showProgress(){
        pd = new ProgressDialog(this);
        pd.setTitle("Cargando...");
        pd.setMessage("Por favor, espere.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.show();
    }

    public void cancelar(View view){
        finish();
    }
    public boolean validateIsAllow(){
        if(titulo.getText().toString().isEmpty()){
            Utils.showDialog(context,"Información","Falta título");
            return false;
        }
        if(nuevo.categoria_id.isEmpty()){
            Utils.showDialog(context,"Información","Falta categoría");
            return false;
        }
        if(descripcion.getText().toString().isEmpty()){
            Utils.showDialog(context,"Información","Falta descripción");
            return false;
        }
        enviar.setEnabled(false);
    
        Call<ResponseAllowZone> responseAllowZoneCall = myGovServices.isAllow(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().longitude,
              mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().latitude);
        responseAllowZoneCall.enqueue(new Callback<ResponseAllowZone>() {
            @Override
            public void onResponse (Call<ResponseAllowZone> call, Response<ResponseAllowZone> response) {
                if(response.isSuccessful()){
                    if(response.body().data.isAllow){
                        crearEvento();
                    } else {
                        Utils.showDialog(context,"Información","No se puede crear el caso en esta zona");
                        enviar.setEnabled(true);
                        return;
                    }
                } else {
                    Gson gson = new Gson();
                    Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
                    enviar.setEnabled(true);
                    Utils.showDialog(context,"Información",error.message);
                    return;
                }
            }
    
            @Override
            public void onFailure (Call<ResponseAllowZone> call, Throwable throwable) {
                enviar.setEnabled(true);
            }
        });
        return false;
    }
    public void crearEvento()
    {
        showProgress();
        pd.show();
        final NewCaseActivity parent =this;
        
        Call<ResponseNuevoEvento2> createEvent = myGovServices.createEvent(sharedPrefs.readSharedSetting("token",""),titulo.getText().toString(),
                                                                            nuevo.categoria_id,descripcion.getText().toString(),
              mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().latitude+"",
                                                                                    mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().longitude+"");
        createEvent.enqueue(new Callback<ResponseNuevoEvento2>() {
            @Override
            public void onResponse(Call<ResponseNuevoEvento2> call, Response<ResponseNuevoEvento2> response) {
                try
                {
                    Log.e("NEWEVENTACTIVITY","RESPONSE NUEVO EVENTO TRY");
                    final int max=images.size();
                    Log.e("NEWEVENTACTIVITY","SIZE:"+images.size());
                    for(int y=0;y<images.size();y++)
                    {
                        Log.e("NEWEVENTACTIVITY","CARGANDO IMAGENES ..."+ String.valueOf(y));
                        Bitmap ima = BitmapFactory.decodeFile(images.get(y).getPath());
                        String im = encodeToBase64(ima, Bitmap.CompressFormat.JPEG,50);
                        final int aux=y;
                        Call<ResponseImage> uploadImage = myGovServices.createImg(sharedPrefs.readSharedSetting("token",""),response.body().data.evento_id+"",im);
                        uploadImage.enqueue(new Callback<ResponseImage>() {
                            @Override
                            public void onResponse(Call<ResponseImage> call, Response<ResponseImage> response) {
                                if(aux==images.size()-1)
                                {

                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseImage> call, Throwable t) {
                            }
                        });
                    }
                    pd.dismiss();
                    enviar.setEnabled(true);
                    parent.setResult(RESULT_OK);
                    finish();
                }
                catch (Exception e)
                {
                    enviar.setEnabled(true);
                    Log.d("MyGov",e.toString());
                    pd.dismiss();
                    Toast.makeText(NewCaseActivity.this, "Error creación evento", Toast.LENGTH_SHORT).show();
                    parent.setResult(RESULT_OK);
                    finish();
                }

            }

            @Override
            public void onFailure(Call<ResponseNuevoEvento2> call, Throwable t) {
                pd.dismiss();
                enviar.setEnabled(true);
                Toast.makeText(NewCaseActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                parent.setResult(RESULT_OK);
                finish();
            }
        });

    }
  
    
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public String imageHandler(Bitmap realImage) {
        float ratio = Math.min(
                (float) 600 / realImage.getWidth(),
                (float) 400 / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, true);

        String imgData = encodeToBase64(newBitmap, Bitmap.CompressFormat.JPEG, 50);
        return imgData;
    }

    public void initRetrofit()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.server))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myGovServices = retrofit.create(MyGovServices.class);
    }
    public void upImages(View view){
        final NewCaseActivity parent =this;

        Log.e("NEWEVENTACTIVITY","RESPONSE NUEVO EVENTO TRY");
        final int max=images.size();
        Log.e("NEWEVENTACTIVITY","SIZE:"+images.size());
        for(int y=0;y<images.size();y++)
        {
            Log.e("NEWEVENTACTIVITY","CARGANDO IMAGENES ..."+ String.valueOf(y));

            //pd.setMessage("Cargando imágenes...");
            Bitmap ima = BitmapFactory.decodeFile(images.get(y).getPath());
            String im = encodeToBase64(ima, Bitmap.CompressFormat.JPEG,50);
            final int aux=y;
            Call<ResponseImage> uploadImage = myGovServices.createImg(sharedPrefs.readSharedSetting("token",""),"18bmm",im);
            uploadImage.enqueue(new Callback<ResponseImage>() {
                @Override
                public void onResponse(Call<ResponseImage> call, Response<ResponseImage> response) {
                    if(aux==images.size()-1)
                    {
                        //pd.dismiss();
                        Toast.makeText(NewCaseActivity.this, "Nuevo Evento creado", Toast.LENGTH_SHORT).show();
                        parent.setResult(RESULT_OK);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<ResponseImage> call, Throwable t) {

                }
            });
        }
    }
}
