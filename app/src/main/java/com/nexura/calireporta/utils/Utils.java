package com.nexura.calireporta.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.nexura.calireporta.R;
import com.nexura.calireporta.api.MyGovServices;

import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Utils {
   static Retrofit retrofit;
   
   public static boolean isValidEmail(String email) {
      Pattern pattern = Patterns.EMAIL_ADDRESS;
      return !TextUtils.isEmpty(email) && pattern.matcher(email).matches();
   }
   
   public static void showToast(Context context, String message){
      Toast.makeText(context, message, Toast.LENGTH_LONG).show();
   }
   
   public static void showDialog(Context context, String title,String message){
      AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
      alertDialog.setTitle(title);
      alertDialog.setMessage(message);
      alertDialog.setCancelable(false);
      alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
         @Override
         public void onClick (DialogInterface dialog, int which) {
            dialog.dismiss();
         }
      });
      alertDialog.show();
   }
   public static Intent setIntent(Context context, Class destination){
      Intent intent = new Intent(context,destination);
      context.startActivity(intent);
      return intent;
   }
   
   public static MyGovServices initRetrofit(Context context)
   {
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      
      OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
      retrofit = new Retrofit.Builder()
            .baseUrl(context.getResources().getString(R.string.server))
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
      return retrofit.create(MyGovServices.class);
   }
}
