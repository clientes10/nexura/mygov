package com.nexura.calireporta.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.nexura.calireporta.R;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.activities.LoginActivity;
import com.nexura.calireporta.adapters.MyCasesAdapter;
import com.nexura.calireporta.adapters.RecentAdapter;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.app.MyApplication;
import com.nexura.calireporta.models.EventoSingle;
import java.util.List;
import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyCasesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyCasesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyCasesFragment extends Fragment {
    String email_string;
    LoginButton loginButton;
    CallbackManager callbackManager;
    private ProgressDialog dialog;
    Retrofit retrofit;
    MyGovServices myGovServices;
    SharedPrefs sharedPrefs;
    private int LOGIN_REQUEST=111;
    private String mParam1;
    private String mParam2;
    RecentAdapter recentAdapter;
    ListView lv;
    private RecyclerView mRecycler_myevents;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyCasesAdapter mAdapter_myevents;
    private LinearLayout container_login;
    private LinearLayout container_eventos;
    private FancyButton btn_login;
    private OnFragmentInteractionListener mListener;
    private TextInputEditText email,password;
    public MyCasesFragment () {
        // Required empty public constructor
    }

    public static MyCasesFragment newInstance() {
        MyCasesFragment fragment = new MyCasesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_my_events, container, false);
        final List<EventoSingle> lista =MyApplication.getMisEventos();
        sharedPrefs = new SharedPrefs(getContext());
        initRetrofit();
        btn_login = (FancyButton) root.findViewById(R.id.login);
        dialog = new ProgressDialog(getContext());
        container_login = (LinearLayout)root.findViewById(R.id.login_container);
        container_eventos = (LinearLayout) root.findViewById(R.id.container_eventos);
        mRecycler_myevents = (RecyclerView)root.findViewById(R.id.recycler_myevents);
        mRecycler_myevents.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler_myevents.setLayoutManager(mLayoutManager);
        mAdapter_myevents = new MyCasesAdapter(getContext(),lista);
        mRecycler_myevents.setAdapter(mAdapter_myevents);
        dialog.setTitle("Cargando");
        dialog.setMessage("Por favor espere ... ");
        if(!sharedPrefs.readSharedSetting("logged",false)){
            container_login.setVisibility(View.VISIBLE);
            container_eventos.setVisibility(View.GONE);
        }else{
            container_login.setVisibility(View.GONE);
            container_eventos.setVisibility(View.VISIBLE);
        }
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setIntent(getContext(), LoginActivity.class);
            }
        });
        callbackManager = CallbackManager.Factory.create();



        return root;
    }

    public void refresh()
    {
        mAdapter_myevents = new MyCasesAdapter(getContext(), MyApplication.getMisEventos());
        mRecycler_myevents.setAdapter(mAdapter_myevents);
        if(!sharedPrefs.readSharedSetting("logged",false)){
            container_login.setVisibility(View.VISIBLE);
            container_eventos.setVisibility(View.GONE);
        }else{
            container_login.setVisibility(View.GONE);
            container_eventos.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String action);
    }

    public void initRetrofit() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        retrofit = new Retrofit.Builder().baseUrl(getResources().getString(R.string.server))
                .client(new OkHttpClient.Builder()
                        .addInterceptor(interceptor).build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myGovServices = (MyGovServices) retrofit.create(MyGovServices.class);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    
}
