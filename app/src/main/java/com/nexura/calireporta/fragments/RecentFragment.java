package com.nexura.calireporta.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.adapters.RecentCasesAdapter;
import com.nexura.calireporta.app.MyApplication;
import com.nexura.calireporta.R;

public class RecentFragment extends Fragment {

    SharedPrefs sharedPrefs;
    ListView lv;
    private OnFragmentInteractionListener mListener;
    RecentCasesAdapter mAdapter_recentsevent;
    RecyclerView mRecycler_recentsevent;
    private RecyclerView.LayoutManager mLayoutManager;
    public RecentFragment() {
    }

    public static RecentFragment newInstance() {
        RecentFragment fragment = new RecentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_recent, container, false);

        mRecycler_recentsevent =(RecyclerView) root.findViewById(R.id.recycler_recentsevent);
        mRecycler_recentsevent.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler_recentsevent.setLayoutManager(mLayoutManager);
        mAdapter_recentsevent = new RecentCasesAdapter(getContext(),MyApplication.getEventos());
        mRecycler_recentsevent.setAdapter(mAdapter_recentsevent);


        sharedPrefs = new SharedPrefs(getContext());
        return root;
    }

    public void refresh()
    {
        if(sharedPrefs.readSharedSetting("isFiltered",false))
        {
            mAdapter_recentsevent = new RecentCasesAdapter(getContext(), MyApplication.getEventos());
            mRecycler_recentsevent.setAdapter(mAdapter_recentsevent);
        }
        else
        {
            mAdapter_recentsevent = new RecentCasesAdapter(getContext(), MyApplication.getEventos());
            mRecycler_recentsevent.setAdapter(mAdapter_recentsevent);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
