package com.nexura.calireporta.presenters;

import android.app.Activity;

import com.facebook.CallbackManager;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.CompleteListener;
import com.nexura.calireporta.views.LoginView;

public class LoginPresenter implements CompleteListener {
   LoginView view ;
   LoginInteractor interactor;
   
   public LoginPresenter(LoginView view, Activity context){
      this.view = view;
      this.interactor = new LoginInteractor(this);
      this.interactor.context = context;
   }
   
   @Override
   public void onSuccess () {
      if(view != null){
         view.hideProgress();
         view.onNavigateHome();
      }
   }
   
   @Override
   public void onSuccess (String message) {
   
   }
   
   @Override
   public void onError (String error) {
      if(view != null){
         view.hideProgress();
         view.setLoginError(error);
      }
   }
   
   public void attemptLogin(String username, String password){
      if(view != null){
         view.showProgress();
         view.setUsernameError("");
         view.setPasswordError("");
         if(username.isEmpty()){
            view.setUsernameError("Campo username vacío");
            view.hideProgress();
            return;
         }
         if(!Utils.isValidEmail(username)){
            view.setUsernameError("Ingresa un email valido");
            view.hideProgress();
            return;
         }
   
         if(password.isEmpty()){
            view.setPasswordError("Campo de contraseña vacío");
            view.hideProgress();
            return;
         }
         interactor.performLogin(username,password);
      }

   }
   
   public void attemptLoginFacebook(CallbackManager callbackManager){
      if(view != null){
         if(callbackManager != null){
            interactor.performLoginFacebook(callbackManager);
         }
      }
   }
   
   public void attemptLoginGoogle(GoogleSignInClient mGoogleSigninClient){
      if(view != null){
         interactor.performLoginGoogle(mGoogleSigninClient);
      }
   }
   public void attemptRegisterLoginApiGoogle(String nombre, String apellido, String email, String token_google){
      if(view != null){
         interactor.performRegisterLoginApiGoogle(nombre, apellido, email, token_google);
      }
   }
   
   public void onDestroy(){
      view = null;
   }
}
