package com.nexura.calireporta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by ervic on 7/3/18.
 */

public class ResponseComentarios {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("comentarios")
    @Expose
    private List<Comentario> comentarios = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("success", success).append("comentarios", comentarios).toString();
    }
}
