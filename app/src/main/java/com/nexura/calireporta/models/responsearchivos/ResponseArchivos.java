
package com.nexura.calireporta.models.responsearchivos;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ResponseArchivos implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("archivos")
    @Expose
    private List<Archivo> archivos = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Archivo> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("success", success).append("archivos", archivos).toString();
    }

}
