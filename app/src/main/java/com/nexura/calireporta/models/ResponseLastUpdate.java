package com.nexura.calireporta.models;

/**
 * Created by ervic on 7/3/18.
 */

public class ResponseLastUpdate {

    public String last_update;

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }
}
