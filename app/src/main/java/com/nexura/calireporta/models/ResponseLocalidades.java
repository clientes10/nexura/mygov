package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by ervic on 7/3/18.
 */

public class ResponseLocalidades {

    public boolean success;
    public List<Localidad> localidades;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

}
