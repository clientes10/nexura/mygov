package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 20/03/17.
 */

public class ResponseTipoValoraciones {
    public List<TipoValoracion> tiposValoracion;
    public boolean success;
}
