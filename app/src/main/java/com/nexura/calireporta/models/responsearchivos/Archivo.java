
package com.nexura.calireporta.models.responsearchivos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nexura.calireporta.models.InfoUsuario;

import org.apache.commons.lang3.builder.ToStringBuilder;
public class Archivo implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("usuario_id")
    @Expose
    private String usuarioId;
    @SerializedName("usuario_info")
    @Expose
    private InfoUsuario usuarioInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Archivo withId(String id) {
        this.id = id;
        return this;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Archivo withTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Archivo withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Archivo withUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
        return this;
    }

    public Object getUsuarioInfo() {
        return usuarioInfo;
    }

    public void setUsuarioInfo(InfoUsuario usuarioInfo) {
        this.usuarioInfo = usuarioInfo;
    }

    public Archivo withUsuarioInfo(InfoUsuario usuarioInfo) {
        this.usuarioInfo = usuarioInfo;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("tipo", tipo).append("nombre", nombre).append("usuarioId", usuarioId).append("usuarioInfo", usuarioInfo).toString();
    }

}
