package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 19/03/17.
 */

public class Categoria {
    public int id;
    public String nombre;
    public String descripcion;
    public String icono;
    public List<SubCategoria> subcategorias;
}
