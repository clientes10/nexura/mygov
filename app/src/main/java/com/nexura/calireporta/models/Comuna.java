package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 20/03/17.
 */

public class Comuna {
    public int comuna_id;
    public String comuna_nombre;
    public List<Localidad> localidades;
}
