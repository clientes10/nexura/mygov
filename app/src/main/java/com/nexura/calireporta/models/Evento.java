package com.nexura.calireporta.models;

/**
 * Created by javpoblano on 26/03/17.
 */

public class Evento {
    public int id;
    public String titulo;
    public String descripcion;
    public int cat;
    public int idpadre;
    public String fecha,fechaP,fechaF;
    public String x,y; //y = lat , x = lon
    public String imagen;
    public int eid,lid;
    public TotalValoraciones tV;
    public TotalComentarios tC;
    public String ico;
}
