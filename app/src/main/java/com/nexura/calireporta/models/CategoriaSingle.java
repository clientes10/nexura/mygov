package com.nexura.calireporta.models;

/**
 * Created by javpoblano on 28/03/17.
 */

public class CategoriaSingle {
    public int id;
    public int idpadre;
    public String nombre;
    public String descripcion;
    public String icono;
}
