package com.nexura.calireporta.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nexura.calireporta.fragments.MapFragment;
import com.nexura.calireporta.fragments.MeAfectanFragment;
import com.nexura.calireporta.fragments.MyCasesFragment;
import com.nexura.calireporta.fragments.RecentFragment;

/**
 * Created by javpoblano on 15/03/17.
 */

public class TabAdapter extends FragmentPagerAdapter {

    public MyCasesFragment myCasesFragment;
    public MapFragment mapFragment;
    public RecentFragment recentFragment;
    public MeAfectanFragment meAfectanFragment;

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        //myCasesFragment = MyCasesFragment.newInstance();
        switch (position)
        {
            case 0:
                myCasesFragment = MyCasesFragment.newInstance();
                return myCasesFragment;
            case 1:
                meAfectanFragment = MeAfectanFragment.newInstance();
                return meAfectanFragment;
            case 2:
                mapFragment = MapFragment.newInstance();
                return mapFragment;
            case 3:
                recentFragment = RecentFragment.newInstance();
                return recentFragment;
            default:
                //myCasesFragment = MyCasesFragment.newInstance();
                return myCasesFragment;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SECTION 1";
            case 1:
                return "SECTION 2";
            case 2:
                return "SECTION 3";
            case 3:
                return "Section 4";
        }
        return null;
    }
}
