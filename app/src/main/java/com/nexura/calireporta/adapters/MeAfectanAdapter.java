package com.nexura.calireporta.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nexura.calireporta.views.activities.SingleViewActivity;
import com.nexura.calireporta.models.EventoSingle;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.nexura.calireporta.R;

/**
 * Created by ervic on 12/11/17.
 */

public class MeAfectanAdapter extends RecyclerView.Adapter<MeAfectanAdapter.CardViewafectaEvent> {
    private Context activity;
    private List<EventoSingle> list_myevents;
    private int COMENTARIO_OK = 222;

    public MeAfectanAdapter(Context context, List<EventoSingle> misEventos) {
        this.activity = context;
        this.list_myevents = misEventos;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public CardViewafectaEvent onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_afecta_event,parent,false);
        return new CardViewafectaEvent(v);

    }

    @Override
    public void onBindViewHolder(CardViewafectaEvent holder, final int position) {
        final EventoSingle eventoSingle = list_myevents.get(position);
        holder.itemView.setTag(eventoSingle);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SingleViewActivity.class);
                intent.putExtra("id",eventoSingle.id);
                intent.putExtra("from",3);
                ((Activity)activity).startActivityForResult(intent,COMENTARIO_OK);
            }
        });
        holder.titulo.setText(eventoSingle.titulo);
        holder.lugar.setText(eventoSingle.dir);
        holder.estado.setText("Estado: " + eventoSingle.estado.nombre);
        try
        {
            if(!eventoSingle.archivos.get(0).isEmpty()){
                String server = activity.getResources().getString(R.string.server);
                server=server+eventoSingle.archivos.get(0);
    
                Picasso.with(activity)
                      .load(server)
                      .placeholder(R.drawable.sin_imagen)
                      .error(R.drawable.sin_imagen)
                      .into(holder.imagen);
            } else {
                holder.imagen.setImageDrawable(null);
            }

        }
        catch (Exception e)
        {
            Log.d("MyGov",e.toString());
        }
    }

    @Override
    public int getItemCount() {
        if(list_myevents == null)
            return 0;
        return list_myevents.size();
    }


    public class CardViewafectaEvent extends RecyclerView.ViewHolder{
        protected TextView titulo;
        protected TextView estado;
        protected ImageView imagen;
        protected TextView lugar;




        public CardViewafectaEvent(View itemView) {
            super(itemView);

            titulo = (TextView) itemView.findViewById(R.id.titulo_afe);
            estado = (TextView) itemView.findViewById(R.id.estado_afe);
            imagen = (ImageView) itemView.findViewById(R.id.img_afe);
            lugar = (TextView) itemView.findViewById(R.id.lugar_afe);


        }
    }
}
