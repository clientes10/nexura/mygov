package com.nexura.calireporta.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nexura.calireporta.R;
import com.nexura.calireporta.models.Comentario;

import java.util.List;

/**
 * Created by ervic on 6/27/18.
 */

public class ComentariosAdapter extends RecyclerView.Adapter<ComentariosAdapter.ViewHolder>{
    Context context;
    List<Comentario> lista_comentarios;

    public ComentariosAdapter(Context context, List<Comentario> lista_comentarios) {
        this.context = context;
        this.lista_comentarios = lista_comentarios;
    }

    @Override
    public ComentariosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_comentario,parent,false);

        return new ComentariosAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComentariosAdapter.ViewHolder holder, int position) {
        Comentario comentario = lista_comentarios.get(position);
        holder.tipo.setText(comentario.tipo.contains("C")?"Comentario":"Respuesta");
        if (comentario.tipo.contains("C")){
            holder.tipo.setTextColor(context.getResources().getColor(R.color.colorNegro));
        }else{
            holder.tipo.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        holder.fecha.setText(comentario.fecha);
        holder.mensaje.setText(comentario.mensaje);
    }

    @Override
    public int getItemCount() {
        if(lista_comentarios == null)
            return 0;
        else
            return lista_comentarios.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tipo;
        public TextView mensaje;
        public TextView fecha;

        public ViewHolder(View itemView) {
            super(itemView);
            tipo = (TextView) itemView.findViewById(R.id.comentario_tipo);
            mensaje = (TextView) itemView.findViewById(R.id.comentario_texto);
            fecha = (TextView) itemView.findViewById(R.id.comentario_fecha);

        }
    }
}
