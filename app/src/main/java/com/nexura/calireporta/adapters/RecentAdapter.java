package com.nexura.calireporta.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nexura.calireporta.R;
import com.nexura.calireporta.views.activities.SingleViewActivity;
import com.nexura.calireporta.models.EventoSingle;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by javpoblano on 26/03/17.
 */

public class RecentAdapter extends BaseAdapter {
    private Context activity;
    private LayoutInflater inflater;
    private List<EventoSingle> movieItems;
    int op=0;

    public RecentAdapter(Context activity, List<EventoSingle> movieItems) {
        this.activity = activity;
        this.movieItems = movieItems;
        op=0;
    }

    public RecentAdapter(Context activity, List<EventoSingle> movieItems, int op) {
        this.activity = activity;
        this.movieItems = movieItems;
        this.op = op;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int location) {
        return movieItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_recent, parent,false);

        final int pos = position;
        TextView titulo = (TextView) convertView.findViewById(R.id.titulo);
        TextView lugar = (TextView) convertView.findViewById(R.id.lugar);
        TextView estado = (TextView) convertView.findViewById(R.id.estado);
        ImageView img = (ImageView) convertView.findViewById(R.id.img);
        // getting movie data for the row
        final EventoSingle m = movieItems.get(position);
        //user.setText(m.name);
        titulo.setText(m.titulo);
        //lugar.setText(m.);
        try
        {
            if(!m.archivos.get(0).isEmpty()) {
                String server = activity.getResources().getString(R.string.server);
                server = server + m.archivos.get(0);
    
                Picasso.with(activity)
                      .load(server)
                      .placeholder(R.drawable.sin_imagen)
                      .error(R.drawable.sin_imagen)
                      .into(img);
            } else {
                img.setImageDrawable(null);
            }
        }
        catch (Exception e)
        {
            Log.d("MyGov",e.toString());
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start activity
                Intent i = new Intent(activity, SingleViewActivity.class);
                i.putExtra("id", pos);
                i.putExtra("from",op);
                activity.startActivity(i);
            }
        });

        return convertView;
    }

    
    @Override
    public int getItemViewType(int position) {
        return position;
    }

}